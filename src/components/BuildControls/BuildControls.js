import React from 'react';
import BuildControl from './BuildControl';
import './BuildControls.css';

const controls = [
    {label:'Salad', type:'salad'},
    {label:'Cheese', type:'cheese'},
    {label:'Bacon', type:'bacon'},
    {label:'Aaloo', type:'aaloo'},
];

const BuildControls = (props) => (
<div className={'BuildControls'}>
<p>Your Burger Costs: <strong>Rs.{props.price}</strong></p>
<p className={'Red'}>You can add upto 3 slices...!!</p>
{controls.map((cntrl) => 
(
<BuildControl 
    key={cntrl.label} 
    label={cntrl.label}
    added={() => props.masalaadded(cntrl.type)}
    removed={() => props.masalaremoved(cntrl.type)}
    disabledless={props.disabledless[cntrl.type]}
    disabledmore={props.disabledmore[cntrl.type]}
    />
   
))}
 <br></br>
<button 
    className={'OrderButton'} 
    disabled={!props.purchasable}
    onClick={props.ordered}>{props.isAuth ? 'ORDER NOW' : 'SignUp to Order'}</button>
</div>
)

export default BuildControls;