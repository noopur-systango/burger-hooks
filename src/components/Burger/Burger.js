import React from 'react';
import './Burger.css';
import Burgermasala from '../BurgerMasala/BurgerMasala';
import {withRouter} from 'react-router-dom';

const Burger = (props) => {
    let transformedmasala = Object.keys(props.masala)
    .map(mKey => {
        return [...Array( props.masala[mKey]  )].map( (_, i) => {
            return <Burgermasala key={mKey + i} type={mKey}/>;
        });
    })
    .reduce ((arr,el) => {
        return arr.concat(el)
    }, []);
    if(transformedmasala.length === 0)
    {
        transformedmasala = <p>Start creating your own burger...!!</p>
    }
    //console.log(transformedmasala);
    return (
        <div className={'Burger'}>
            <Burgermasala type="bread-top"></Burgermasala>
            {transformedmasala}
            <Burgermasala type="bread-bottom"></Burgermasala>
        </div>
    );

}

export default withRouter(Burger);