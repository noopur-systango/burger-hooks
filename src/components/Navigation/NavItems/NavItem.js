import React from 'react';
import './NavItems.css';
import {NavLink} from 'react-router-dom'; 

const Navitem = (props) => 
(
    <li className={'NavItem'}>
        <NavLink to={props.link} activeClassName={'active'}>{props.children}</NavLink>
        </li>
);

export default Navitem;