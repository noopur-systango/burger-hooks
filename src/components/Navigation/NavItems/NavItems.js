import React from "react";
import "./NavItem.css";
import NavItem from "./NavItem";

const Navitems = (props) => (
  <ul className={"NavItems"}>
    <NavItem link="/">Home</NavItem>

    {props.isAuthenticated ? 
      <>
        <NavItem link="/orders">Orders</NavItem>
        <NavItem link="/logout">Logout</NavItem>
      </>
     : 
      <NavItem link="/auth">Login</NavItem>
    }
  </ul>
);

export default Navitems;
