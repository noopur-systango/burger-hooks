import React from 'react';
import './Toolbar.css';
import Logo from '../../Logo/Logo';
import NavItems from '../NavItems/NavItems';

const Toolbar = (props) => (
    <header className={'Toolbar'}>
    <Logo height="80%"/>
    <h3 className={'Text'}>BURGER KING</h3>
    <nav>
       <NavItems isAuthenticated={props.isAuth}/>
    </nav>
    </header>
);

export default Toolbar;