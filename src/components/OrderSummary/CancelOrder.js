import React from "react";
import Button from "../../components/UI/Button/Button";


const CancelOrder = (props) => {
  return (
    <div className={"CancelOrder"}>
        <center>
      <h1>Cancel Order</h1>
      <p>Are you sure you want to cancel this order ?</p>
      <Button btnType="Danger" clicked={props.ordercancel}>
        No
      </Button>
      <Button btnType="Success" clicked={props.ordercontinue}>
        Yes
      </Button>
      </center>
    </div>
  );
};

export default CancelOrder;
