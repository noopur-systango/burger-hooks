import React from 'react';
import Burger from '../Burger/Burger';
import './CheckoutSummary.css';


const Checkoutsummary = (props) => {
    return(
        <div className={'CheckoutSummary'}>
        <h1>Your Burger is ready..!!</h1>
        <div className={'Burgerr'}>
            <Burger masala={props.masala}/>
        </div>
        </div>
    );
}

export default Checkoutsummary;