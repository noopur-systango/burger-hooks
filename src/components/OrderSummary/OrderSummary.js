import React from "react";
import Aux from "../../hoc/Aux/Aux";
import Button from "../../components/UI/Button/Button";

const Ordersummary = props => {
  const masalasummary = Object.keys(props.masala).map(mKey => {
    return (
      <div key={mKey} >
        <span style={{ textTransform: "capitalize" }}> {mKey}</span> :{" "}
        {props.masala[mKey] + " Slices "}
      </div>
    );
  });
  return (
    <Aux>
      <center>
      <h3>Your Order</h3>
      <p>A delicious burger for you is here with :</p>
       {masalasummary}
      <p>
        <strong>Total Price: Rs.{props.price}</strong> 
      </p>
      <p>Continue to Checkout?</p>
      <Button btnType="Danger" clicked={props.purchasecancel}>
        Cancel
      </Button>
      <Button btnType="Success" clicked={props.purchasecontinue}>
        Continue
      </Button>
      </center>
    </Aux>
  );
};

export default Ordersummary;
