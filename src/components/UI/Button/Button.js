import React from 'react';

import './Button.css';

const Button = (props) => (
    <button
        className={props.btnType==='Success'? 'Button Success': 'Button Danger'}
        onClick={props.clicked} type={props.type || "button"}>{props.children}</button>
);

export default Button; 