import React from 'react';
import './Input.css';
import Aux from '../../../hoc/Aux/Aux';

const Input = (props) => 
{  
    return(
        <Aux>
        <div>
            <input 
            className={props.invalid && props.shouldValidate && props.touched ? 'InputElement Invalid' : 'InputElement'} 
            inputtype="input" 
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.changed}
            name={props.name}
            type={props.type}
            /><br></br>
        </div>  
        {props.invalid && <div className={'ValidationError'}>
        {props.errorMessage}
        </div>}
        </Aux>  
    );
}

export default Input;