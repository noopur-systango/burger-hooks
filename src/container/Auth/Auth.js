import React, { Component } from 'react';
import { DefaultState } from './Constants';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import './Auth.css';
import { checkValidity } from '../Checkout/ContactData/Validation/Validation';
import { auth, setAuthRedirectPath } from '../../store/actions/auth';
import  { connect } from 'react-redux'; 
import Spinner from '../../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router-dom';

class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = DefaultState
      }
    
      componentDidMount() {
        if (!this.props.buildingBurger && this.props.authRedirectPath !== '/') {
            this.props.onSetAuthRedirectPath();
        }
    }

    inputChangedHandler = (event, key) => {
      const errors = {
        ...this.state.errors   //extracts errors...
      };

      const data = checkValidity(event.target.value,this.state.signInForm[key].validation);

      const updatedSignInForm = {
      ...this.state.signInForm,
        [key]: {
            ...this.state.signInForm[key],
            value: event.target.value,
            valid: data.isValid,
            touched:true
            }
        };
       
        if(data.errors){
          errors[key] = data.errors; 
        } 
       this.setState({ signInForm: updatedSignInForm, errors });
    }
      
    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuth(this.state.signInForm.email.value,this.state.signInForm.password.value,this.state.isSignUp)
        
      }

    switchAuthModeHandler = () => {
      this.setState(prevState =>  {
        return { isSignUp: !prevState.isSignUp};
      })
    }


    render() {
    const { signInForm , errors} = this.state;
    let form = (
        <div>
        {Object.keys(signInForm).map(key => (
          <Input
            type={signInForm[key].type}
            key={key}
            name={key}
            placeholder={key}
            changed={event => {
              this.inputChangedHandler(event, key);
            }}
            invalid={!signInForm[key].valid}
            shouldValidate={signInForm[key].validation}
            touched={signInForm[key].touched}
            errorMessage={errors[key]}
          ></Input>
        ))}
        <Button 
        type="submit"
        btnType="Success">Submit</Button><br></br>
        <span>{!this.state.isSignUp ? 'New User ?': null }</span>
        <Button 
        clicked={this.switchAuthModeHandler}
        btnType="Danger">Click Here To {this.state.isSignUp ? 'Login':'SignUp '}</Button>
      </div>
    );

    if(this.props.loading){
      form = <Spinner />
    }     

    let errormessage = null;
    if(this.props.error){
      errormessage = (
      <p className={'Error'}>{this.props.error.message}</p>
      )
    }

    let authRedirect = null;
    if(this.props.isAuthenticated)
    {
      authRedirect = <Redirect to={this.props.authRedirectPath}></Redirect>
    }   
 
        return (
        <div className={"SignIn"}>
          <h4> {this.state.isSignUp ? 'SignUp':'Login '}</h4>
          {authRedirect}  
           <form onSubmit={this.submitHandler}>
            {form}
           </form>
           {errormessage}  
      </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    buildingBurger: state.burgerBuilder.building,
    authRedirectPath: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAuth: (email,password,isSignUp) => dispatch(auth(email, password,isSignUp)),
        onSetAuthRedirectPath: () => dispatch(setAuthRedirectPath('/'))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Auth);