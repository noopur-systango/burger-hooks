export const DefaultState = {
    signInForm: {
        email: {  
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true,
            isEmail: true
          }
        },
        password: {
            type: "password",
            value: "",
            valid: false,
            touched: false,
            validation: {
              required: true,
              isPassword:6,
            }
          },
      },
      isSignUp: false,
      errors: {}
  }