import React, { Component } from 'react';
import {authLogout} from '../../../store/actions/auth';
import { initMasala } from '../../../store/actions/burgerBuilder';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class Logout extends Component {

    componentDidMount () {
        this.props.onInitMasala();
        this.props.onLogout();
    }
    render() {
        return (
          <Redirect to="/"></Redirect>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout:() => dispatch(authLogout()),
        onInitMasala: () => dispatch(initMasala())
    }
}

export default connect(null,mapDispatchToProps)(Logout);