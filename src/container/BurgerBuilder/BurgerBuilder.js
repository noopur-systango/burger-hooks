import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import Spinner from "../../components/UI/Spinner/Spinner";
import * as burgerBuilderActions from '../../store/actions/burgerBuilder' 
import { purchaseInit } from '../../store/actions/order'; 
import { connect } from 'react-redux';
import { setAuthRedirectPath } from "../../store/actions/auth";
import './BurgerBuilder.css';

class BurgerBuilder extends Component {
  state = {
    purchasing: false,
    loading: false
  };

  componentDidMount () {
      if(this.props.buildingBurger && this.props.isAuthenticated){
      this.props.history.push('/checkout/contact-data')
    } else {
       this.props.onInitMasala();
    } 
  }

  updatePurchaseState(masala) {
    const sum = Object.keys(masala)
      .map(mKey => {
        return masala[mKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);
    return sum > 0;
  }

  purchaseHandler = () => {
    if(this.props.isAuthenticated) {
    this.setState({ purchasing: true });
    } else {
      this.props.onSetAuthRedirectPath('/checkout');
      this.props.history.push('/auth');
    }
  };

  purchaseHandlerClose = () => {
    this.setState({ purchasing: false });
  };

  purchaseHandlerContinue = () => {
    this.props.onInitPurchase();
    this.props.history.push('/checkout/contact-data');
  };


  render() {
    const disabledInfoLess = {
      ...this.props.ings
    };
    for (let key in disabledInfoLess) {
      disabledInfoLess[key] = disabledInfoLess[key] <= 0;
    }

    const disabledInfoMore = {
      ...this.props.ings
    };
    for (let key in disabledInfoMore) {
      disabledInfoMore[key] = disabledInfoMore[key] >= 3;
    }
    let ordersummary = null;

    const { purchasing } = this.state;

    let burger = <Spinner />;

    if (this.props) {
      burger = (
        <Aux>
          <Burger masala={ this.props.ings }></Burger>
         <BuildControls
            masalaadded={this.props.onMasalaAdded}
            masalaremoved={this.props.onMasalaRemoved}
            disabledless={disabledInfoLess}
            disabledmore={disabledInfoMore}
            purchasable={ this.updatePurchaseState(this.props.ings)}
            isAuth={this.props.isAuthenticated}
            price={ this.props.price } 
            ordered={this.purchaseHandler}
          />
        </Aux>
      );
      ordersummary = (
        <OrderSummary
          masala={this.props.ings}
          price={this.props.price}
          purchasecancel={this.purchaseHandlerClose}
          purchasecontinue={this.purchaseHandlerContinue}
        />
      );
    }

    if (this.state.loading) {
      ordersummary = <Spinner />;
    }
    return (
      <Aux>
        <Modal
          show={ purchasing }
          modalClosed={this.purchaseHandlerClose}
        >
          {ordersummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

const mapStateToProps = (state) => ({
      ings: state.burgerBuilder.masala,  //passes as props to the component
      price: state.burgerBuilder.totalPrice,
      error: state.burgerBuilder.error,
      isAuthenticated: state.auth.token !== null,
      buildingBurger: state.burgerBuilder.building
   })


const mapDispatchToProps = (dispatch) => ({
      onMasalaAdded: (ingName) => dispatch(burgerBuilderActions.addMasala(ingName)),
      onMasalaRemoved: (ingName) => dispatch(burgerBuilderActions.removeMasala(ingName)),
      onInitMasala: () => dispatch(burgerBuilderActions.initMasala()),
      onInitPurchase: () => dispatch(purchaseInit()),
      onSetAuthRedirectPath: (path) => dispatch(setAuthRedirectPath(path))
     })


export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
