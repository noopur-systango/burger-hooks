import React,{Component} from 'react';
import Checkoutsummary from '../../components/OrderSummary/CheckoutSummary';
import {Route} from 'react-router-dom'; 
import ContactData from '../../container/Checkout/ContactData/ContactData';
import { connect } from 'react-redux';

class Checkout extends Component{

   /*  checkoutCancelHandler = () => {
        this.props.history.goBack();
    }

    checkoutContinueHandler = () => {
        this.props.history.replace('/checkout/contact-data');
    } */

    render(){
        return(
            <div>
                <Checkoutsummary masala={this.props.ings}/>
                 <Route path={this.props.match.path + '/contact-data'} 
                  component={ContactData}></Route>
            </div>
        );
    }
} 

const mapStateToProps = (state) => {
    return {
        ings: state.burgerBuilder.masala,
    }
}

export default connect(mapStateToProps)(Checkout);

