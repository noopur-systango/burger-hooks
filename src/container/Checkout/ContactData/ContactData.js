import React, { Component } from "react";
import Button from "../../../components/UI/Button/Button";
import "./ContactData.css";
import Input from "../../../components/UI/Input/Input";
import { checkValidity } from "../ContactData/Validation/Validation";
import { DefaultState } from "./Constants";
import { connect } from "react-redux";
import { purchaseBurger } from "../../../store/actions/order";
import Spinner from "../../../components/UI/Spinner/Spinner";
import { initMasala } from "../../../store/actions/burgerBuilder";
import CancelOrder from "../../../components/OrderSummary/CancelOrder";
import Modal from "../../../components/UI/Modal/Modal";
import Aux from "../../../hoc/Aux/Aux";

class ContactData extends Component {
  constructor(props) {
    super(props);
    this.state = DefaultState;
  }

  cancelOrder = () => {
    this.setState({ isModalOpen: true });
  };

  orderHandler = (event) => {
    event.preventDefault();
    const { orderForm } = this.state;
    const order = {
      masala: this.props.ings,
      price: this.props.price,
      orderData: { orderForm }, //sending form data to server
      userId: this.props.userId,
    };

    this.props.onOrderBurger(order, this.props.token);
    this.props.history.push("/orders");
  };

  orderCancelHandler = () => {
    this.setState({ isModalOpen: false });
  }

  orderContinueHandler = () => {
    window.location.href = '/'
  }

  inputChangedHandler = (event, key) => {
    const updatedOrderForm = {
      ...this.state.orderForm, //extracts name,number....
    };

    const errors = {
      ...this.state.errors, //extracts errors...
    };

    const updatedFormElement = {
      ...updatedOrderForm[key], //extracts "value" of each key
    };

    updatedFormElement.value = event.target.value;

    //checking validation & setting valid, touched to true
    const data = checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation
    );

    updatedFormElement.touched = true;
    updatedFormElement.valid = data.isValid;
    if (data.errors) {
      errors[key] = data.errors;
    }

    updatedOrderForm[key] = updatedFormElement;
    this.setState({ orderForm: updatedOrderForm, errors });
  };

  render() {
    const { orderForm, errors } = this.state;
    let form = (
      <form onSubmit={this.orderHandler}>
        {Object.keys(orderForm).map((key) => (
          <Input
            key={key}
            name={key}
            placeholder={" Your " + key}
            changed={(event) => {
              this.inputChangedHandler(event, key);
            }}
            invalid={!orderForm[key].valid}
            shouldValidate={orderForm[key].validation}
            touched={orderForm[key].touched}
            errorMessage={errors[key]}
          ></Input>
        ))}
        <Button btnType="Danger" clicked={this.cancelOrder}>
          Cancel
        </Button>
        <Button btnType="Success" clicked={this.orderHandler}>
          Order
        </Button>
      </form>
    );
    if (this.props.loading) {
      form = <Spinner />;
    }
    let cancelButton = (
      <CancelOrder
        ordercancel={this.orderCancelHandler}
        ordercontinue={this.orderContinueHandler}
      ></CancelOrder>
    );

    return (
      <Aux>
        <div className={"ContactData"}>
          <h4>Enter your contact data:</h4>
          {form}
        </div>
        <Modal
          show={this.state.isModalOpen}
          modalClosed={this.orderCancelHandler}
        >
          {cancelButton}
        </Modal>
      </Aux>
    );
  }
}

const mapStateToProps = (state) => ({
  ings: state.burgerBuilder.masala, //passes as props to the component
  price: state.burgerBuilder.totalPrice,
  loading: state.order.loading,
  token: state.auth.token,
  userId: state.auth.userId,
});

const mapDispatchToProps = (dispatch) => {
  return {
    onOrderBurger: (orderData, token) =>
      dispatch(purchaseBurger(orderData, token)),
    onInitMasala: () => dispatch(initMasala()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);
