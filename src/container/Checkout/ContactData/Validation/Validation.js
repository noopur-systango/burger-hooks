export function checkValidity(value, rules) {
    let isValid = true;
    let errors = [];

    if(!rules){
        return true;
    }

    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
      if(!isValid)
      errors = "This field is required!";
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
      if(!isValid)
      errors = "Please enter a valid 10-digit number!";
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
      if(!isValid)
      errors = "Please enter a valid 10-digit number!";
    }

    if (rules.isPassword) {
      isValid = value.length >= rules.isPassword && isValid;
      if(!isValid)
      errors = "Password must be 6 characters long!";
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
      if(!isValid)
      errors = "Please enter valid email-ID!";
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
      if(!isValid)
      errors = "Please enter a valid 10-digit number!"
    }

    return {isValid,errors};
}