import React, { Component } from "react";
import Order from "../../components/Order/Order";
import { connect } from 'react-redux'
import Spinner from '../../components/UI/Spinner/Spinner'
import { fetchOrders } from '../../store/actions/order';
import noOrder from '../../assests/images/noOrder.png';
import './Orders.css';

class Orders extends Component {
   
    componentDidMount() {
       this.props.onFetchOrders(this.props.token,this.props.userId);
    }

    renderOrder = () => {
        return this.props.orders.map(order => (
            <Order 
            key={order.id}
            masala={order.masala}
            price={order.price}/>
      ))
    }
 
    renderNoOrder = () => {
        return (<div><img className={'Image'} src={noOrder} alt="No Order..."/></div>)
    }

  render() {
      let order= null;
    return (
        <div>
         {this.props.loading ?  <Spinner/> : 
         (this.props.orders.length ? this.renderOrder() : this.renderNoOrder() )  }
         {order}
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        orders: state.order.orders,
        loading:state.order.loading,
        token:state.auth.token,
        userId:state.auth.userId
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchOrders: (token,userId) => dispatch(fetchOrders(token,userId))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Orders);
