import { AUTH_START, AUTH_SUCCESS, AUTH_FAIL, AUTH_LOGOUT, SET_AUTH_REDIRECT_PATH } from './actionConst';
import axios from 'axios';
import { toast } from 'react-toastify'; 
import { SIGN_UP, SIGN_IN } from '../../Constants/apiURL';

export const authStart = () => {
    return {
        type: AUTH_START
    }
}

export const authSuccess = (token, userId) => {
    return {
        type: AUTH_SUCCESS,
        idToken: token,
        userId: userId
    }
}

export const authFail = (error) => {
    return {
        type: AUTH_FAIL,
        error: error
    }
}

export const authLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: AUTH_LOGOUT,
    }
}

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout( () => {
            dispatch(authLogout());
        },expirationTime * 1000)
    }
}

export const auth = (email,password, isSignup) => {
    return  dispatch => {
        const authData = {
            email:email,
            password:password,
            returnSecureToken:true
        }
        dispatch(authStart());
        let url = SIGN_UP
        if(!isSignup)
        {
            url = SIGN_IN
        }
        
        axios.post( url,authData)
        .then( response => {
            const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000)      
            localStorage.setItem('token', response.data.idToken);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.data.localId); 
            dispatch(authSuccess(response.data.idToken,response.data.localId))
            toast.success("Login Success..!", {
                position: toast.POSITION.TOP_RIGHT
              });
            dispatch(checkAuthTimeout(response.data.expiresIn))       
        })
        .catch(error => {
            console.log(error)
            dispatch(authFail(error.response.data.error))
            toast.error("Login Failed ..!", {
                position: toast.POSITION.TOP_RIGHT
              });
        }) 
           
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type: SET_AUTH_REDIRECT_PATH,
        path:path
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(authLogout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(authLogout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
            }   
        }
    }
}