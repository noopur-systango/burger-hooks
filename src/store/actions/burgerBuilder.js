import * as actionTypes from './actionConst';
import axios from '../../axios-config';
import { GET_MASALA } from '../../Constants/apiURL';

export const addMasala = (name) => {
    return {
        type: actionTypes.ADD_MASALA,
        masalaName: name
    }
}

export const removeMasala = (name) => {
    return {
        type: actionTypes.REMOVE_MASALA,
        masalaName: name
    }
}

export const setMasala = (masala) => {
    return {
        type: actionTypes.SET_MASALA,
        masala: masala
    }
}

export const fetchMasalaFailed = () => {
    return {
        type: actionTypes.FETCH_MASALA_FAILED,
    }
}

export const initMasala = () => {
    return dispatch => {
        axios
        .get(GET_MASALA)
        .then(response => {
          dispatch(setMasala(response.data));
        })
        .catch(error => {
          dispatch(fetchMasalaFailed()); 
        });
    }
}