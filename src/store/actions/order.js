import * as actionTypes from './actionConst';
import axios from '../../axios-config';
import { initMasala } from './burgerBuilder';
import { toast } from 'react-toastify'; 
import { ORDER } from '../../Constants/apiURL';

export const purchaseBurgerSuccess = (id, orderData) => {
    return {
        type:actionTypes.PURCHASE_BURGER_SUCCESS,
        orderid: id,
        orderData: orderData
    }
}

export const purchaseBurgerFail = (error) => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAIL,
        error: error
    }
}

export const purchaseBurgerStart = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START,
    }
}

export const purchaseBurger = (orderData,token) => {
    return dispatch => {
        dispatch(initMasala())
        dispatch (purchaseBurgerStart());
        axios
        .post(ORDER + '?auth=' + token, orderData)
        .then(response => {
          dispatch (purchaseBurgerSuccess(response.data.userId, orderData));
          toast.success("Order Success..!", {
            position: toast.POSITION.TOP_RIGHT
          });
        })
        .catch(error => {
          dispatch (purchaseBurgerFail(error));
          toast.error("Order Failed ..!", {
            position: toast.POSITION.TOP_RIGHT
          });
        });
    };
}

export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT
    }
}

export const fetchOrderSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDER_SUCCESS,
        orders:orders
    }
}

export const fetchOrderFail = (error) => {
    return {
        type: actionTypes.FETCH_ORDER_FAIL,
        error:error
    }
}

export const fetchOrderStart = () => {
    return {
        type: actionTypes.FETCH_ORDER_START
    }
}

export const fetchOrders = (token, userId) => {
    return dispatch => {
        dispatch(fetchOrderStart());
        const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
        axios.get(ORDER + queryParams)
        .then(res => {
            const fetchedOrders = [];
            for(let key in res.data)
            {
                fetchedOrders.push({...res.data[key],id:key});
            }
            dispatch(fetchOrderSuccess(fetchedOrders))
        })
        .catch(error => {
            dispatch(fetchOrderFail(error));
        })
    };
}
