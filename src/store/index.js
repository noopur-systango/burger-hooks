import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk';
import burgerBuilderReducer from './reducers/burgerBuilder'
import orderReducer from './reducers/order'
import authReducer from './reducers/auth';


const rootReducer = combineReducers({
    burgerBuilder :burgerBuilderReducer,
    order: orderReducer,
    auth: authReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
//middleware is one of composeEnhancers
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;