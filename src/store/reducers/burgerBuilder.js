import { ADD_MASALA, REMOVE_MASALA, SET_MASALA, FETCH_MASALA_FAILED } from "../actions/actionConst";

const initialState = {
  masala: {
    salad: 0,
    cheese: 0,
    aaloo: 0,
    bacon: 0
  },
  error: false,
  totalPrice: 10,
  building:false
};

const masala_prices = {
    salad: 10,
    cheese: 20,
    aaloo: 30,
    bacon: 40
  };

const burgerBuilderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MASALA:
      return {
        ...state,
        masala: {
          ...state.masala,
          [action.masalaName]: state.masala[action.masalaName] + 1
        },
        totalPrice: state.totalPrice + masala_prices[action.masalaName],
        building:true
      };
    case REMOVE_MASALA:
      return {
        ...state,
        masala: {
          ...state.masala,
          [action.masalaName]: state.masala[action.masalaName] - 1
        },
        totalPrice: state.totalPrice - masala_prices[action.masalaName],
        building:true
      };
    case SET_MASALA:
      return {
        ...state,
        masala: action.masala,
        totalPrice: 10,
        error: false,
        building:false
      }; 
    case FETCH_MASALA_FAILED:
      return {
          ...state,
          error:true
      }; 
      default:
          return state;
  }
}

export default burgerBuilderReducer;